-- 创建数据库
CREATE DATABASE IF NOT EXISTS  `blog_ssm` CHARACTER SET utf8 COLLATE utf8_general_ci;

-- 使用blog_ssm库
USE blog_ssm;

-- 创建文章分类表
CREATE TABLE IF NOT EXISTS `article_category` (
  `id` int(4) NOT NULL AUTO_INCREMENT COMMENT 'ID编号',
  `name` varchar(50) NOT NULL COMMENT '类别名称' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章类别表';
-- 创建文章表
CREATE TABLE IF NOT EXISTS `article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID编号',
  `title` varchar(100) NOT NULL COMMENT '文章的标题',
  `content` text NOT NULL COMMENT '文章的内容',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章表';