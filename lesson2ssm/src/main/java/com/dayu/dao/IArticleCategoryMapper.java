package com.dayu.dao;

import com.dayu.model.ArticleCategoryModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

/**
 * Created by dayu on 2017/4/26 0026.
 * Email：yzjs@qq.com
 * mapper映射文件配置之insert、update、delete
 */
@Repository
public interface IArticleCategoryMapper {

    int insert(ArticleCategoryModel model);

    ArrayList<ArticleCategoryModel> getAll();

    int deleteById(@Param(value = "id") Integer id);

    ArticleCategoryModel getModelById(@Param(value = "id") Integer id);

    int update(ArticleCategoryModel model);
}
