package com.dayu.model;

/**
 * Created by dayu on 2017/4/26 0026.
 * Email：yzjs@qq.com
 */
public class ArticleCategoryModel {

    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
