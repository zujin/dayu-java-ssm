package com.dayu.service.impl;

import com.dayu.dao.IArticleCategoryMapper;
import com.dayu.model.ArticleCategoryModel;
import com.dayu.service.IArticleCategoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * Created by dayu on 2017/4/26 0026.
 * Email：yzjs@qq.com
 */
@Service("articleCategoryService")
public class ArticleCategoryServiceImpl implements IArticleCategoryService {

    @Resource
    private IArticleCategoryMapper iArticleCategoryMapper;

    public int insert(ArticleCategoryModel model) {
        return iArticleCategoryMapper.insert(model);
    }

    public ArrayList<ArticleCategoryModel> getAll() {
        return iArticleCategoryMapper.getAll();
    }

    public int deleteById(Integer id) {
        return iArticleCategoryMapper.deleteById(id);
    }

    public ArticleCategoryModel getModelById(Integer id) {
        return iArticleCategoryMapper.getModelById(id);
    }

    public int update(ArticleCategoryModel model) {
        return iArticleCategoryMapper.update(model);
    }
}
