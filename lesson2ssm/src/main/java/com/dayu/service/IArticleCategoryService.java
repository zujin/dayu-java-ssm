package com.dayu.service;

import com.dayu.model.ArticleCategoryModel;

import java.util.ArrayList;

/**
 * Created by dayu on 2017/4/26 0026.
 * Email：yzjs@qq.com
 */
public interface IArticleCategoryService {

    int insert(ArticleCategoryModel model);

    ArrayList<ArticleCategoryModel> getAll();

    int deleteById(Integer id);

    ArticleCategoryModel getModelById(Integer id);

    int update(ArticleCategoryModel model);
}
