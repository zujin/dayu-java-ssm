<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--在JSP页面中使用JSTL标签库：
1、引入jstl.jar 和standard.jar两个jar包
2、在jsp页面加入 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>，如若需要引用其他标签，可将URI值换成对应地址。--%>

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Lesson 2</title>
    <link href="${basePath}style/main.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="${basePath}js/jquery-3.2.1.min.js"></script>
</head>
<body>
<h1>文章类别的增、删、改、查演示</h1>
<div class="form-box">
    类别名称：<input type="text" id="name" name="name"/><input type="button" id="subAddData" value="提交"
                                                          onclick="addOrEditData();"/>
    <input type="hidden" id="hdfId" value="0">
</div>

<!--加一个Table显示数据-->
<div class="table-box">
    <table border="0" cellpadding="0" cellspacing="1">
        <tr>
            <th>ID</th>
            <th>分类名称</th>
            <th>操作</th>
        </tr>
        <c:forEach items="${articleCategoryModels}" var="model">
            <tr>
                <td><c:out value="${model.id}"/></td>
                <td><c:out value="${model.name}"/></td>
                <td><a href="javascript:<c:out value="deleteData(${model.id})"/>">删除</a> &nbsp;&nbsp; <a
                        href="javascript:<c:out value="getEditData(${model.id})"/>">修改类别</a></td>
            </tr>
        </c:forEach>
    </table>
</div>

<script type="text/javascript">

    /**
     * 添加或修改文章的分类
     */
    var addOrEditData = function () {
        var id = $("#hdfId").val();
        var name = $("#name").val();
        if (name.length == 0) {
            alert("请输入分类名称！");
            return;
        }
        var intId = parseInt(id);
//        alert(intId);
        if (intId > 0) {
            // 执行Ajax修改数据
            $.ajax({
                type: "POST",
                url: "${basePath}article-category/update",
                data: {id: intId, name: name},
                success: function (data) {
                    if (data == "success") {
                        alert("分类信息修改成功～！");
                        location.href = "${basePath}article-category/list";
                    } else {
                        alert("出错了：" + data);
                    }
                },
                error: function () {
                    alert("系统错误～！");
                }
            });
        } else {
            // 执行Ajax添加数据
            $.ajax({
                type: "POST",
                url: "${basePath}article-category/create",
                data: {name: name},
                success: function (data) {
                    if (data == "success") {
                        alert("分类信息添加成功～！");
                        location.href = "${basePath}article-category/list";
                    } else {
                        alert("出错了：" + data);
                    }
                },
                error: function () {
                    alert("系统错误～！");
                }
            });
        }
    };

    /**
     * 删除一条文章分类信息
     */
    var deleteData = function (dataId) {
        if (confirm("确定要删除吗？")) {
            // 执行Ajax删除
            $.ajax({
                type: "GET",
                url: "${basePath}article-category/delete/" + dataId,
                success: function (data) {
                    if (data == "success") {
                        location.href = "${basePath}article-category/list";
                    } else {
                        alert("出错了：" + data);
                    }
                },
                error: function () {
                    alert("系统错误～！");
                }
            });
        }
    };

    /**
     * 获取修改的文章分类信息
     * @param dataId
     */
    var getEditData = function (dataId) {
        // 获取一条文章分类数据
        $.ajax({
            type: "GET",
            url: "${basePath}article-category/" + dataId,
            dataType: "json",
            success: function (data) {
                console.log("返回的数据：" + data);
                $("#hdfId").val(data.id);
                $("#name").val(data.name);
            }
        });
    };

</script>
</body>
</html>
