# 使用IntelliJ IDEA2017开发基于Maven+Spring MVC的入门Java项目


####什么是MVC？MVC（Model-View-Controller）三元组的概念：

>Spring Mvc是基于MVC的一个框架，是一种设计模式，是开发软件程序的一种解决方案  。

Model（模型）：数据模型，提供要展示的数据，因此包含数据和行为，可以认为是领域模型或JavaBean组件（包含数据和行为），不过现在一般都分离开来：Value Object（数据） 和 服务层（行为）。也就是模型提供了模型数据查询和模型数据的状态更新等功能，包括数据和业务。

View（视图）：负责进行模型的展示，一般就是我们见到的用户界面，客户想看到的东西。

Controller（控制器）：接收用户请求，委托给模型进行处理（状态改变），处理完毕后把返回的模型数据返回给视图，由视图负责展示。 也就是说控制器做了个调度员的工作。

####Spring Mvc执行流程：

用户在客户端发送request请求到C（control 接收用户请求响应）控制器，控制器再通过M模型（pojo、action、service、dao）层处理得到结果，然后控制器把结果经过视图渲染，最后返回至终端（response）。

1、发起请求到前端控制器（DispatcherServlet ）。

2、前端控制器请求HandlerMapping查找Handler（可以根据xml、注解进行查找）。

3、处理器映射器HandlerMapping向前端控制器DispatcherServlet 返回Handler。

4、前端控制器DispatcherServlet 调用处理器适配器HandlerAdapter 执行Handler。

5、处理器适配器HandlerAdapter 执行Handler。

6、Handler执行完给处理器适配器返回ModelAndView。

7、处理器适配器向前端控制器返回ModelAndView （ModelAndView 是SpringMvc的底层对象 包括model和view）。

8、前端控制器请求视图解析器去解析视图，根据逻辑视图名解析成真正的视图（jsp）。

9、视图解析器向前端控制器返回view。

10、前端控制器进行视图渲染，视图渲染将模型数据（模型数据在ModelAndView对象中）填充到request域。

11、前端控制器向用户响应结果。

####Maven是什么? 为什么要使用Maven：

Maven是一个目前比较流行的可跨平台、可实施标准化构建的工具（利器）。可以标准化构建过程，使所有的项目简单一致，简化学习成本。

>Maven能干什么？

1、依赖的管理：仅仅通过jar包的几个属性，就能确定唯一的jar包，在指定的文件pom.xml中，只要写入这些依赖属性，就会自动下载并管理jar包。

2、项目的构建：内置很多的插件与生命周期，支持多种任务，比如校验、编译、测试、打包、部署、发布...

3、项目的知识管理：管理项目相关的其他内容，比如开发者信息，版本等等

####开发环境搭建

> 开发环境：IntelliJ IDEA 2017 + GIT + MAVEN + Spring MVC 4.3.7 + Tomcat8

1、下载安装JDK1.8

2、下载安装Tomcat8

3、下载安装Intellij IDEA

### 一、修改pom.xml文件，加载 Spring MVC相关的依赖Jar包

相关Jar包版本号声明

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <jdk.version>1.8</jdk.version>
        <!-- spring版本号 -->
        <spring.version>4.3.7.RELEASE</spring.version>
        <!-- mybatis版本号 -->
        <mybatis.version>3.4.4</mybatis.version>
        <!-- log4j日志文件管理包版本 -->
        <slf4j.version>1.7.25</slf4j.version>
        <log4j.version>1.2.17</log4j.version>
    </properties>
    
> 说明：基于Spring mvc框架进行开发，需要依赖一下的Spring Jar包：

`spring-aop-4.0.4.RELEASE.jar
spring-beans-4.0.4.RELEASE.jar
spring-context-4.0.4.RELEASE.jar
spring-core-4.0.4.RELEASE.jar
spring-expression-4.0.4.RELEASE.jar
spring-web-4.0.4.RELEASE.jar
spring-webmvc-4.0.4.RELEASE.jar
commons-logging-1.1.1.jar（用来打印log）`

### 二、添加 HelloWorldController 控制器
前端通过 @RequestMapping 中匹配的URL地址请求，执行注解 @RequestMapping 在类级别和方法级别的业务逻辑处理后返回ModelAndView。


### 三、添加静态文件夹、项目入口文件index.jsp

1、在webapp目录下建立index.jsp作为项目默认入口文件。

2、在webapp目录下建立3个文件夹用于放静态的文件：images[用于放图片]，js[用于放js文件]，style[用于放css文件]。


### 四、添加视图控制器HelloWorldController对应的展示页面 suc.jsp 
在WEB-INF下面建立一个文件夹views，用于放jsp文件。 我们这里先建立一个HelloWorldController中需要引用的jsp文件：suc.jsp。

### 五、在 resources 目录下添加 Spring Config 文件 spring-content.xml
在本项目中spring-content.xml的主要作用：1、配置自动扫描的包，2、配置视图解析器，把handler方法返回值解析为实际的物理视图

### 六、配置WEB-INF下的web.xml文件
web.xml文件声明了一个Servlet（即dispatcher servlet）来接收所有类型的请求。Dispatcher servlet在这里充当前端控制器的角色。

### 七、配置Tomcat
目前比较流行的Web应用服务器Tomcat： 技术先进、性能稳定，而且免费。